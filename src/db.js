const dbConfig = require('./config/db.config');
const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');

//db credentials
const sequelize = new Sequelize(dbConfig.DB,dbConfig.USER,dbConfig.PASSWORD,{
    host:dbConfig.HOST,
    dialect:dbConfig.dialect,
    pool:{
        max:dbConfig.pool.max,
        min:dbConfig.pool.min,
        acquire:dbConfig.pool.acquire,
        idle:dbConfig.pool.idle,
    }
});

const basename = path.basename(__filename);

const modelDefiners = [];

//sequelize setup
fs.readdirSync(path.join(__dirname, '/models'))
  .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    modelDefiners.push(require(path.join(__dirname, '/models', file)));
  });


  modelDefiners.forEach(model => model(sequelize));

  let entries = Object.entries(sequelize.models);
  let capsEntries = entries.map((entry) => [entry[0][0].toUpperCase() + entry[0].slice(1), entry[1]]);
  sequelize.models = Object.fromEntries(capsEntries); 
  
  const { Post, User, Categories} = sequelize.models;
//sequelize Relations
User.hasMany(Post)
Post.belongsTo(User)
Categories.hasMany(Post)
Post.belongsTo(Categories)


module.exports ={
    ...sequelize.models,
    conn: sequelize,
}

