const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
 
    sequelize.define('post', {
      postId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        unique:true,
        autoIncrement:true
    },
      categoryId: {
        type: DataTypes.INTEGER,
        references:{
            model:'categories',
            key:'id'
        }
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references:{
            model:'users',
            key:'id'
        }
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false
      },
      body:{
        type: DataTypes.TEXT,
        allowNull: false
      },
      headerImg: {
        type: DataTypes.STRING,
        allowNull:true
      },
      published: {
        type: DataTypes.STRING,
        allowNull:true
      },

    });

  };