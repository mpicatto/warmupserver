const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
 
    sequelize.define('user', {
      email: {
        type: DataTypes.STRING,
        unique:true
      },
      password: {
        type: DataTypes.STRING
      },
      name: {
        type: DataTypes.STRING
      },
      lastName: {
        type: DataTypes.STRING
      },

    });
  };