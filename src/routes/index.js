const {Router}= require('express');
//import routers
const router = Router();
const users=require('./users');
const posts=require('./posts') 

//load routers on routes
router.use('/users', users)
router.use('/posts', posts)

module.exports =router;