//libraries
const moment =require('moment');
const server = require('express').Router();
const {  User,Categories,Post} = require('../db');
const  { hash } = require( 'bcryptjs');
const multer = require('multer');


const FILE_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpeg',
    'image/jpg': 'jpg'
}

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const isValid = FILE_TYPE_MAP[file.mimetype];
        let uploadError = new Error('invalid image type');

        if(isValid) {
            uploadError = null
        }
      cb(uploadError, 'public/uploads')
    },
    filename: function (req, file, cb) {
        
      const fileName = file.originalname.split(' ').join('-');
      const extension = FILE_TYPE_MAP[file.mimetype];
      cb(null, `${fileName}-${Date.now()}.${extension}`)
    }
  })
  
const uploadOptions = multer({ storage: storage })

server.get('/',getCategories,getAllPosts);
async function getCategories(req,res,next){
    let data ={posts:[],categoryList:[]}
    await Categories.findAll({})
    .then(category=>{
        category.map(item=>{
            data.categoryList.push(item.dataValues)
        })
    })
    .catch(err=>{
        res.send("No se encuentran las categorias")
        return
    })
   
    req.data = data
    console.log(req.data)
    next()
}


async function getAllPosts(req, res, next){
    await Post.findAll({
        include:User,
        attributes: { exclude: ['body'] }            
    })
    .then(posts=>{
        console.log(posts)
        posts.map(item=>{
            req.data.posts.push(item.dataValues)
        })
        res.json(req.data)
    })
    .catch(err=>{
        res.send("No se encuentra el post ")
        return
    }) 
}

server.get('/:id',(req,res)=>{
    console.log(req.params.id)
    let id= parseInt(req.params.id)
    Post.findByPk(id,{
        include:Categories,
    })
    .then(posts=>{
        res.json(posts)
    })
    .catch(err=>{
        res.send("No se encuentra el post ")
        return
    }) 
})

server.post(`/`, async (req, res) =>{
    
    console.log(req.body)
    let {
        categoryId,
        userId,
        title,
        body,
        headerImg,
     } = req.body;

     categoryId=parseInt(categoryId)
     let published = moment().format("YYYY-MM-DD")


     Post.create({
 
        categoryId,
        userId,
        title,
        body,
        headerImg,
        published
      })
      .then(user=>{
          return res.sendStatus(201);
      })
      .catch(err=>{
          res.sendStatus(500)
      })

})

server.put(`/:id`, async (req, res) =>{

    let{categoryId,
        title,
        body,
        headerImg,} = req.body

    console.log(title)    
    let id= parseInt(req.params.id)
    Post.findByPk(id)
    .then(posts=>{
        posts.categoryId=categoryId;
        posts.title=title;
        posts.body=body;
        posts.headerImg=headerImg;
        posts.save()
    })
    .catch(err=>{
        res.send("No se encuentra el post ")
        return
    })
    res.send("ok") 

})

server.delete('/:id', (req,res)=>{
    let id= parseInt(req.params.id)
    Post.findByPk(id)
    .then(posts=>{
        posts.destroy()
    })
    res.send("ok")
})
module.exports = server;